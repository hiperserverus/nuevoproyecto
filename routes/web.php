<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','FrontController@index');

Route::get('/perfil/{nombre}', [

	'uses' => 'FrontController@perfil',

	'as' => 'usuario.perfil'


	]);

Route::put('/perfil/{nombre}', [

	'uses' => 'FrontController@perfil_update',

	'as' => 'usuario.update'

	]);

Route::get('/cursos','FrontController@cursos');

Route::group(['prefix' => 'cursos'], function () {

Route::get('/categorias/{nombre}',[


	'uses' => 'FrontController@buscarCategoria', 
	'as' => 'buscar.categoria'

	]);

Route::get('/autor/{name}',[

	'uses' => 'FrontController@buscarAutores',
	'as' => 'buscar.autores'


	]);



});


Route::get('/cursos/{slug}',[

	'uses' => 'FrontController@contenido',
	'as' => 'curso.contenido'

	]);

Route::get('/politicas','FrontController@politicas');

Route::get('/terminos','FrontController@terminos');



Auth::routes();


Route::get('/home', 'HomeController@index');

//RUTAS DE ADMINISTRADOR

Route::group(['prefix' => 'administrador', 'middleware' => 'auth'], function () {

//-->AQUI SE PUEDEN COLOCAR RUTAS DE ACCCESO ADMINISTRATIVO PARA USUARIOS MIEMBRO

	Route::group(['middleware'=>'administrador'], function() {

					Route::resource('usuarios','UsuariosController');

					Route::get('/usuarios/{id}/destroy',[

						'uses' => 'UsuariosController@destroy',
						'as'   => 'administrador.usuarios.destroy'

						]);
					Route::resource('categorias','CategoriasController');

					Route::get('/categorias/{id}/destroy',[

						'uses' => 'CategoriasController@destroy',
						'as'   => 'administrador.categorias.destroy'

						]);

					Route::resource('tags','TagsController');

					Route::get('/tags/{id}/destroy',[

						'uses' => 'TagsController@destroy',
						'as'   => 'administrador.tags.destroy'

						]);

					Route::resource('/cursos','CursosController');

					Route::get('/cursos/{id}/destroy',[

						'uses' => 'CursosController@destroy',
						'as'   => 'administrador.cursos.destroy'

						]);

					Route::get('/imagenes',[

						'uses' => 'ImagenesController@index',
						'as'   => 'administrador.imagenes.index'

						]);

			Route::group(['prefix' => 'curso/{id_curso}'], function () {

					Route::resource('/contenidos','ContenidosController');

					Route::get('/contenidos/{id_contenido}/destroy',[

						'uses' => 'ContenidosController@destroy',
						'as'   => 'administrador.contenidos.destroy'

						]);

								Route::group(['prefix' => 'contenido/{id_contenido}'], function () {

											Route::resource('/capitulos','CapitulosController');

											Route::get('/capitulos/{id_capitulo}/destroy',[

											'uses' => 'CapitulosController@destroy',
											'as'   => 'administrador.capitulos.destroy'

									]);

				});

				});

	});



});

