@extends('layouts.app')

@section('title','Categorias')

@section('header')

@include('partials.header')

@endsection

@section('content')

<div class="view-school">

		<section id="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs">
						<nav class="breadcrumb">
							
							<a href="{{route('usuarios.index')}}" class="breadcrumb-item">/ Usuarios</a>
							<span class="breadcrumb-item active">/ Categorias</span>
							<a href="{{route('cursos.index')}}" class="breadcrumb-item">/ Cursos</a>
							<a href="{{route('administrador.imagenes.index')}}" class="breadcrumb-item">/ Imagenes</a>
							<a href="{{route('tags.index')}}" class="breadcrumb-item">/ Tags</a>
							
						</nav>
					</div>
				</div>
			</div>
		</section>

<div class="admin-panel">

<h4>Categorias registradas</h4>

<table class="table table-striped">	
		<thead>
			<th>ID</th>
			<th>Nombre</th>
			<th>Accion</th>
		</thead>
		<tbody>
			@foreach($categorias as $categoria)
				<tr>
					<td>{{$categoria->id}}</td>
					<td>{{$categoria->nombre}}</td>
					<td>
						<a class="btn btn-warning" href="{{route('categorias.edit',$categoria->id)}}"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
						<a class="btn btn-danger" href="{{route('administrador.categorias.destroy',$categoria->id)}}" onclick="return confirm('¿Seguro que deeas Eliminar este usuario')"><i class="fa fa-trash" aria-hidden="true"></i></a>

					</td>
				</tr>
			@endforeach
		</tbody>
</table>

<a class="btn btn-primary" href="{{route('categorias.create')}}">Crear nueva categoria</a>

<div class="text-center" >{!!$categorias->links()!!}</div>


</div>
</div>
@endsection

@section('footer')

@include('partials.footer')

@endsection
