@extends('layouts.app')

@section('title','Editar Tag')

@section('header')

@include('partials.header')

@endsection

@section('content')

<div class="view-school">

		<section id="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs">
						<nav class="breadcrumb">

							<a href="{{route('tags.index')}}" class="breadcrumb-item">Tags</a>
							<span class="breadcrumb-item active">/ Edicion de Tags</span>
						</nav>
					</div>
				</div>
			</div>
		</section>

<div class="admin-panel">

<h4>Editar Tag: ("{{$tag->nombre}}")</h4>

{!!Form::open(['route' => ['tags.update',$tag], 'method' => 'PUT'])!!}

<div class="form-group">
	{!!Form::label('nombre','Nombre')!!}
	{!!Form::text('nombre', $tag->nombre, ['class' => 'form-control', 'placeholder'=> 'Nombre del tag' ,'required'])!!}
</div>


<div class="form-group text-center">
	{!!Form::submit('Editar', ['class' => 'btn btn-primary '])!!}
</div>

{!!Form::close()!!}

</div>
</div>

@endsection

@section('footer')

@include('partials.footer')

@endsection
