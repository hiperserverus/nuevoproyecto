@extends('layouts.app')

@section('title','Imagenes')

@section('header')

@include('partials.header')

@endsection

@section('content')

<div class="view-school">

		<section id="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs">
						<nav class="breadcrumb">
							
							<a href="{{route('usuarios.index')}}" class="breadcrumb-item">/ Usuarios</a>
							<a href="{{route('categorias.index')}}" class="breadcrumb-item">/ Categorias</a>
							<a href="{{route('cursos.index')}}" class="breadcrumb-item">/ Cursos</a>
							<span class="breadcrumb-item active">/ Imagenes</span>
							<a href="{{route('tags.index')}}" class="breadcrumb-item">/ Tags</a>
							
						</nav>
					</div>
				</div>
			</div>
		</section>

<div class="admin-panel">

<h4>Imagenes registradas</h4>

<div class="row">
	@foreach($imagenes as $imagen)

	<div class="col-md-4">
		
		<div class="panel panel-default">
			<div class="panel-body">
				<img src="{{url('/images/cursos/')}}/{{ $imagen->nombre }}" class="img-responsive">
			</div>
			<div class="panel-footer">{{$imagen->curso->nombre}}</div>
		</div>

	</div>

	@endforeach
</div>
			
</div>
			<div class="text-center" >{!!$imagenes->links()!!}</div>
</div>

@endsection

@section('footer')

@include('partials.footer')

@endsection