@extends('layouts.app')

@section('title','Cursos')

@section('header')

@include('partials.header')

@endsection

@section('content')

<div class="view-school">

		<section id="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs">
						<nav class="breadcrumb">
							
							<a href="{{route('usuarios.index')}}" class="breadcrumb-item">/ Usuarios</a>
							<a href="{{route('categorias.index')}}" class="breadcrumb-item">/ Categorias</a>
							<span class="breadcrumb-item active">/ Cursos</span>
							<a href="{{route('administrador.imagenes.index')}}" class="breadcrumb-item">/ Imagenes</a>
							<a href="{{route('tags.index')}}" class="breadcrumb-item">/ Tags</a>
							
						</nav>
					</div>
				</div>
			</div>
		</section>

<div class="admin-panel">

<div class="row">
	<div class="col-md-4 col-xs-12">
		<h4>Cursos registrados</h4>
	</div>




 <!-- CAJA DE BUSQUEDAS -->

      {!!Form::open(['route' => 'cursos.index', 'method' => 'GET' , 'class' => 'col-lg-4 col-md-4 col-xs-12 pull-right'])!!}

     
       
            <div class="input-group">

			{!!Form::text('nombre' ,null ,['class' => 'form-control search input-lg', 'placeholder' => 'Buscar Curso'])!!}
			
            <span class="input-group-btn">
            
              <button id="search-course-button" class="btn search btn-default btn-lg" type="submit"><i class='fa fa-search'></i></button>
           
            </span>
          </div>
      
     

      {!!Form::close()!!}

      </div>
    
   <!-- CAJA DE BUSQUEDAS -->

<table class="table table-striped">	
		<thead>
			<th>ID</th>
			<th>Nombre del Curso</th>
			<th>Categoria</th>
			<th>Usuario</th>
			<th>Tipo de Curso</th>
			<th>Accion</th>
		</thead>
		<tbody>
			@foreach($cursos as $curso)
				<tr>
					<td>{{$curso->id}}</td>
					<td><a href="{{route('contenidos.index',[$curso->id])}}">{{$curso->nombre}}</a></td>
					<td>{{$curso->Categoria->nombre}}</td>
					<td>{{$curso->User->name}}</td>
										<td>
						
						@if($curso->type == "normal")
							<span class="label label-primary">{{$curso->type}}</span>
						@else
							<span class="label label-danger">{{$curso->type}}</span>
						@endif


					</td>
					<td>
						<a class="btn btn-warning" href="{{route('cursos.edit',$curso->id)}}"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
						<a class="btn btn-danger" href="{{route('administrador.cursos.destroy',$curso->id)}}" onclick="return confirm('¿Seguro que deeas Eliminar este usuario')"><i class="fa fa-trash" aria-hidden="true"></i></a>

					</td>

				</tr>
			@endforeach
		</tbody>
</table>

<a class="btn btn-primary" href="{{route('cursos.create')}}">Crear nuevo curso</a>

<div class="text-center" >{!!$cursos->links()!!}</div>


</div>
</div>

@endsection

@section('footer')

@include('partials.footer')

@endsection
