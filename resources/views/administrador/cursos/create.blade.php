@extends('layouts.app')

@section('title','Crear Curso')

@section('css')
<link href="{{ url('plugins/chosen/chosen.css') }}" rel="stylesheet">
<link href="{{ url('plugins/trumbowyg/dist/ui/trumbowyg.css') }}" rel="stylesheet">
@endsection

@section('header')

@include('partials.header')

@endsection

@section('content')

<div class="view-school">

		<section id="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs">
						<nav class="breadcrumb">

							<a href="{{route('cursos.index')}}" class="breadcrumb-item">Cursos</a>
							<span class="breadcrumb-item active">/ Creacion de cursos</span>
						</nav>
					</div>
				</div>
			</div>
		</section>

<div class="admin-panel">

<h4>Crear nuevo curso</h4>

{!!Form::open(['route' => 'cursos.store', 'method' => 'POST', 'files'=> true])!!}

<div class="form-group">
	{!!Form::label('nombre','Nombre del Curso')!!}
	{!!Form::text('nombre', null, ['class' => 'form-control', 'placeholder'=> 'Nombre del curso' ,'required'])!!}
</div>

<div class="form-group">
	{!!Form::label('category_id','Categoria')!!}
	{!!Form::select('category_id', $categorias, null, ['class' => 'form-control select-category',  'required'])!!}
</div>

<div class="form-group">
	{!!Form::label('descripcion','Descripcion')!!}
	{!!Form::textarea('descripcion', null, ['class' => 'form-control textarea-content', 'required'])!!}
</div>

<div class="form-group">
	{!!Form::label('type','Tipo de Curso')!!}
	{!!Form::select('type', ['normal' => 'Normal','destacado' => 'Destacado'],null,['class' => 'form-control', 'placeholder' => 'Seleccione una opción...', 'required'])!!}
</div>

<div class="form-group">
	{!!Form::label('autor_id','Autor')!!}
	{!!Form::select('autor_id', $autores, null, ['class' => 'form-control select-category',  'required'])!!}
</div>

<div class="form-group">
	{!!Form::label('tags','Tags')!!}
	{!!Form::select('tags[]', $tags, null, ['class' => 'form-control select-tag', 'multiple' ,'required'])!!}
</div>

<div class="form-group">
	{!!Form::label('imagen','Imagen')!!}
	{!!Form::file('imagen')!!}
</div>

<div class="form-group text-center">
	{!!Form::submit('Registrar', ['class' => 'btn btn-primary '])!!}
</div>

{!!Form::close()!!}

</div>

</div>

@endsection

@section('footer')

@include('partials.footer')

@endsection

@section('js')

	<script src="{{ url('plugins/chosen/chosen.jquery.js') }}"></script>
	<script src="{{ url('plugins/trumbowyg/trumbowyg.js') }}"></script>

<script>
	$('.select-tag').chosen({
			placeholder_text_multiple: 'Seleccione un máximo de 3 tags',
			max_selected_options: 3,
			search_contains: true,
			no_results_text: 'No se encontraron tags'
	});

	$('.select-category').chosen();

	$('.textarea-content').trumbowyg();

	
</script>


@endsection
