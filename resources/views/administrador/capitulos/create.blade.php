@extends('layouts.app')

@section('title','Crear Capitulo')

@section('header')

@include('partials.header')

@endsection

@section('content')


<div class="view-school">

		<section id="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs">
						<nav class="breadcrumb">
							<a href="{{route('cursos.index')}}" class="breadcrumb-item">Cursos</a>
							<a href="{{route('contenidos.index',$curso_id)}}" class="breadcrumb-item">/ Contenidos</a>
							<a href="{{route('capitulos.index',[$curso_id,$contenido_id])}}" class="breadcrumb-item">/ Capitulos</a>
							<span class="breadcrumb-item active">/ Creacion de capitulos</span>
						</nav>
					</div>
				</div>
			</div>
		</section>

<div class="admin-panel">

<h4>Crear nuevo capitulo</h4>

{!!Form::open(['route' => ['capitulos.store',$curso_id,$contenido_id], 'method' => 'POST'])!!}

<div class="form-group">
	{!!Form::label('nombre','Nombre del capitulo')!!}
	{!!Form::text('nombre', null, ['class' => 'form-control', 'placeholder'=> 'Nombre del capitulo' ,'required'])!!}
</div>

<div class="form-group">
	{!!Form::label('link','Link de Descarga')!!}
	{!!Form::text('link', null, ['class' => 'form-control', 'placeholder'=> 'Link de descarga'])!!}
</div>

<div class="form-group">
	{!!Form::label('status','Estado del Link')!!}
	{!!Form::select('status', ['pendiente' => 'Pendiente','activo' => 'Activo'],null,['class' => 'form-control', 'placeholder' => 'Seleccione una opción...', 'required'])!!}
</div>


<div class="form-group text-center">
	{!!Form::submit('Registrar', ['class' => 'btn btn-primary '])!!}
</div>

{!!Form::close()!!}

</div>
</div>

@endsection

@section('footer')

@include('partials.footer')

@endsection
