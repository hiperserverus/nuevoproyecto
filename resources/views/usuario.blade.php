@extends('layouts.app')

@section('title','Perfil')

@section('header')

@include('partials.header')

@endsection

@section('content')

<div class="view-school">

<div class='course-sidebar lecture-page collapse navbar-collapse navbar-sidebar-collapse'>

  <!-- Sidebar Nav -->
  <ul class='sidebar-nav'>
    <li class="selected">
      <a href="{{route('usuario.perfil',Auth::user()->name)}}">
        <span class='lecture-sidebar-icon'>
          <i class='fa fa-user'></i>
        </span>
        Editar Perfil
      </a>
    </li>

  </ul>
</div>

<div class="course-mainbar edit-settings-page">
  <h2 class='section-title'>
    <i class="fa fa-user"></i>
    Editar Perfil
  </h2>


  

  {!!Form::open([ 'route'=> ['usuario.update',$usuario->name], 'method' => 'PUT'])!!}

      <div class="row form-group">
        
        {!!Form::label('name','Nombre de usuario', ['class' => 'col-md-3 control-label'])!!}
        
        <div class="col-md-9 control-input">
          {!!Form::text('name', $usuario->name, ['class' => 'form-control', 'placeholder'=> 'Nombre de usuario' ,'required'])!!}
          <span class="input-icon"></span>
        </div>
      </div>
      <div class="row form-group">
          {!!Form::label('email','Correo Electrónico', ['class' => 'col-md-3 control-label'])!!}
        </label>
        <div class="col-md-9 control-input">
            {!!Form::text('email', $usuario->email, ['class' => 'form-control', 'placeholder'=> 'Correo Electrónico' ,'required'])!!}
          <span class="input-icon"></span>
        </div>
      </div>
      <div class="row form-group">
         {!!Form::label('password','Contraseña', ['class' => 'col-md-3 control-label'])!!}
        </label>
        <div class="col-md-9 control-input">
          {!!Form::text('password', null, ['class' => 'form-control', 'placeholder'=> '************'])!!}
          <span class="input-icon"></span>
        </div>
      </div>


    <br><br>
    <button class="btn btn-primary btn-hg" id="edit-user-submit" type="submit">
      Atualizar perfil
    </button>
{!!Form::close()!!}

</div>

</div>

@endsection

@section('footer')

@include('partials.footer')

@endsection
