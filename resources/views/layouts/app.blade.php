<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="Descarga gratis los mejores cursos premium de desarrollo web">
	<meta name="robots" content="index, follow">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield('title', 'Default')</title>

	<!-- Styles -->
	<link rel="shortcut icon" href="{{ url('images/logo.png') }}">
	
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1
	/css/font-awesome.min.css" rel="stylesheet">

	<link href="{{ url('css/app.css') }}" rel="stylesheet">
	@yield('css')

	<!-- Scripts -->
	
	<script>
		window.Laravel = <?php echo json_encode([
			'csrfToken' => csrf_token(),
		]); ?>
	</script>

	
</head>

<body>

<!--header -->

<header>

@yield('header')
	
</header>

<!-- /header -->

@include('flash::message')
@include('partials.errors')
	
				
@yield('content')



	@yield('footer')



  <!-- Scripts -->
  <script src="{{ url('js/app.js') }}"></script>
  <!--<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
  integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="
  crossorigin="anonymous"></script>-->
  <script src="{{ url('js/jquery-3.1.1.min.js') }}"></script>
	@yield('js')

</body>
</html>
