@extends('layouts.app')

@section('title','Vekingo')

@section('header')

@include('partials.header')

@endsection

@section('content')


        <!-- HOMEPAGE -->
<!-- HERO Version 1: Site headline, description & call to action -->
<div class="view-school">
<div class='homepage-hero'>
  <div class='container'>
    <div class='row text-center'>
      <h2 class='headline'>Cursos Premium Gratis</h2>
      <h3 class='subtitle'>Descarga cursos premium de programacion web totalmente gratis</h3>
      <br><br><a class='btn-primary btn-hg text-center' href="{{ url('/register') }}">Registrate ahora</a>
    </div>
  </div>
</div>



  <div class='container'>
    
      <h2 class='homepage-section-title'>Cursos Destacados</h2>
      
        <!-- Course Listing -->

          @foreach($cursos as $curso)

          <div class='col-xs-12 col-sm-6 col-md-4'>

          <div data-course-id="103968" data-course-url="{{route('curso.contenido',$curso->slug)}}", class='course-listing' >
            <div class='course-listing' >

              <div class='row'>
                <a href="{{route('curso.contenido',$curso->slug)}}"  data-role="course-box-link">
                  <div class='col-lg-12'>
                    <!-- IMAGEN DEL CURSO, NOMBRE DEL CURSO Y DESCRIPCION -->
                    <div class='course-box-image-container'>
                    @foreach($curso->imagenes as $imagen)
                      <img class='course-box-image' src="{{url('/images/cursos/')}}/{{ $imagen->nombre }}" role="presentation">
                    @endforeach
                    </div>
                    <div class='course-listing-title'>
                      {{$curso->nombre}}
                    </div>
                    <!-- BARRA PROGRESIVA (VISIBLE PARA USUARIOS LOGEADOS) -->
                    <div class='col-xs-12 hidden'>
                      <div class='progressbar'>
                        <div class='progressbar-fill'></div>
                      </div>
                    </div>
                    <!-- DESCRIPCION (PARA USUARIOS NO LOGEADOS) -->
                    <div class='course-listing-subtitle'>
                    {{$curso->descripcion}}
                    </div>
                  </div>
                </a>
              </div>
              <div class='course-listing-extra-info col-xs-12'>
                <div class='pull-left'>
                  <!-- INFORMACION  (SIEMPRE VISIBLE) -->
                  
                  <!-- IMAGEN Y NOMBRE DEL AUTOR (SIEMPRE VISIBLE) -->
                  <img align='left' class='img-circle' src="{{url('/images/avatars/')}}/{{$curso->autor->avatar}}" alt="Willian Justen">
                  <div class='small course-author-name'>
                    {{$curso->autor->nombre}}
                  </div>
                  
                </div>
                <!-- BARRA PROGRESIVA (VISIBLE PARA USUARIOS LOGEADOS) -->
                <div class='pull-right hidden'>
                  <div class='small course-progress'>
                    <span class='percentage' >
                      %
                    </span>
                    <br>
                    COMPLETO
                  </div>
                </div>
                <!-- PRECIO (VISIBLE PARA USUARIOS NO LOGEADOS) -->
                <div class='pull-right'>
                  <div class='small course-price'>
                    GRATIS
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
              @endforeach

      

      
    
  </div>

  <!--/cursos destacados-->

     <!--boton todos los cursos-->

  <center>
      <a class='btn btn-lg btn-primary' href="{{ url('/cursos') }}">Ver todos los Cursos</a>
    </center>
  
  <br>
  <br>
  <br>

<!--/boton todos los cursos-->

    
    <!--Descripciones del sitio-->


  <div class="container props-container">
  <div class="col-md-6 props">
    <div class="value-prop-title">Cursos de Javascript</div>

     <div class="value-prop-subtitle">Podras encontrar y descargar los mejores cursos de javascript de la web, cursos de las mejores plataformas online y totalmente gratis.</div>
  </div>
  <div class="col-md-6 props">
    <div class="value-prop-title">Cursos de Android</div>
    
    <div class="value-prop-subtitle">Tenemos cursos de desarollo en android premium disponibles para descargar, podras aprender a crear aplicaciones android nativas desde cero.</div>
  </div>
  <div class="col-md-6 props">
    <div class="value-prop-title">Cursos de IOS</div>
    
    <div class="value-prop-subtitle">Si te interesa el desarrollo para IOS podras descargar gratis los mejores cursos premium de las mejores plataformas de la web.</div>
  </div>
  <div class="col-md-6 props">
    <div class="value-prop-title">¡Cursos premium de las mejores plataformas!</div>
    <div class="value-prop-subtitle">Descarga cursos premium gratis, en cuentra cursos de <strong>C , C++, PYTHON, RUBY, PHP, JAVA, JAVA EE, GO.</strong> Tenemos todos los cursos de las plataformas que buscas.</div>
  </div>
</div>

 <!--/Descripciones del sitio-->


 <!--BIografia-->

 <div class="course-block bio odd-stripe" id="block-388205">
      <div class="container">
  <div class="row">
    <div class="col-xs-1 visible-xs"></div>
    <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
  
      <br>
      <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-4 text-center">
          <img class="large-img-rounded img-responsive" src="{{url('images/yo.jpg')}}" alt="Willian Justen">
        </div>
        <div class="col-lg-9 col-md-8 col-sm-8">
          <p>Esta plataforma fue diseñada para compartir cursos premium online que no puede ser adquirido por una gran cantidad de personas, de tal manera que lo que se busca es masificar el conocimiento y que a su vez el apoyo sea reciproco para seguir llevandoles a todos ustedes mucho mas contenido de calidad.Descarga gratis y disfruta¡¡
</p>
        </div>
      </div>
      <br>
    </div>
  </div>
</div>

    </div>


<!--/BIografia-->


            <!-- Donaciones -->

            <div class="donation-buttons">  
                <p>Ayudanos a tener mas cursos: </p>

                
               <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
            <input type="hidden" name="cmd" value="_donations">
            <input type="hidden" name="business" value="twd00026@gmail.com">
            <input type="hidden" name="lc" value="VE">
            <input type="hidden" name="item_name" value="tecnocracia digital">
            <input type="hidden" name="no_note" value="0">
            <input type="hidden" name="currency_code" value="USD">
            <input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
            <img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1">
            </form>

            </div>

            </div>

            <!-- /Donaciones -->
            


@endsection

@section('footer')

@include('partials.footer')

@endsection
