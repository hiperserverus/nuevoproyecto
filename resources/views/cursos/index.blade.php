@extends('layouts.app')

@section('title','Cursos de desarrollo web premium totalmente gratis')

@section('header')

@include('partials.header')

@endsection

@section('content')

<div class="view-school">
<div class='view-directory course-directory '> 
  <div class='container'>
    





    <div class='row search'>

      
         <!-- FILTRAR CURSOS POR CATEGORIA -->

        <div class='pull-left course-filter'>
          <div class="filter-label">
            Categoria:
          </div>
          <div class="btn-group">
            <button class="btn btn-default btn-lg btn-course-filter dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
              
                Todos <span class="caret"></span>
              
            </button>
            <ul class="dropdown-menu" role="menu">
              <li><a href="{{url('/cursos')}}">Todos</a></li>
              @foreach($categorias as $categoria)
              <li><a href="{{route('buscar.categoria',$categoria->nombre)}}">{{$categoria->nombre}}</a></li>
              @endforeach
              </ul>
          </div>
        </div>
<!-- /FILTRAR CURSOS POR CATEGORIAS -->

        <!-- FILTRAR CURSOS POR AUTOR -->

        <div class='pull-left course-filter'>
          <div class="filter-label">
            Autor:
          </div>
          <div class="btn-group">
            <button class="btn btn-default btn-lg btn-course-filter dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
              
                Todos <span class="caret"></span>
              
            </button>
            <ul class="dropdown-menu" role="menu">

              <li><a href="{{url('/cursos')}}">Todos</a></li>

              @foreach($autores as $autor)
                <li><a href="{{route('buscar.autores',$autor->nombre)}}">{{$autor->nombre}}</a></li>
              @endforeach
            </ul>
          </div>
        </div>

 <!-- /FILTRAR CURSOS POR AUTOR -->
      

 <!-- CAJA DE BUSQUEDAS -->

      {!!Form::open(['url' => '/cursos', 'method' => 'GET' , 'class' => 'col-lg-4 col-md-4 col-xs-12 pull-right'])!!}

     
       
            <div class="input-group">

      {!!Form::text('nombre' ,null ,['class' => 'form-control search input-lg', 'placeholder' => 'Buscar Curso'])!!}
      
            <span class="input-group-btn">
            
              <button id="search-course-button" class="btn search btn-default btn-lg" type="submit"><i class='fa fa-search'></i></button>
           
            </span>
          </div>
      
     

      {!!Form::close()!!}
    
   <!-- CAJA DE BUSQUEDAS -->

<!--/BLOQUE DE BUSQUEDA-->

          <!--LISTA DE CURSOS-->

          @foreach($cursos as $curso)

          <div class='col-xs-12 col-sm-6 col-md-4'>

          <div data-course-url="{{route('curso.contenido',$curso->slug)}}", class='course-listing' >
            <div class='course-listing' >

              <div class='row'>
                <a href="{{route('curso.contenido',$curso->slug)}}" >
                  <div class='col-lg-12'>
                    <!-- IMAGEN DEL CURSO, NOMBRE DEL CURSO Y DESCRIPCION -->
                    <div class='course-box-image-container'>
                    @foreach($curso->imagenes as $imagen)
                      <img class='course-box-image' src="{{url('/images/cursos/')}}/{{ $imagen->nombre }}" role="presentation">
                    @endforeach
                    </div>
                    <div class='course-listing-title'>
                      {{$curso->nombre}}
                    </div>
                    <!-- BARRA PROGRESIVA (VISIBLE PARA USUARIOS LOGEADOS) -->
                    <div class='col-xs-12 hidden'>
                      <div class='progressbar'>
                        <div class='progressbar-fill'></div>
                      </div>
                    </div>
                    <!-- DESCRIPCION (PARA USUARIOS NO LOGEADOS) -->
                    <div class='course-listing-subtitle'>
                    {{$curso->descripcion}}
                    </div>
                  </div>
                </a>
              </div>
              <div class='course-listing-extra-info col-xs-12'>
                <div class='pull-left'>
                  <!-- INFORMACION  (SIEMPRE VISIBLE) -->
                  
                  <!-- IMAGEN Y NOMBRE DEL AUTOR (SIEMPRE VISIBLE) -->
                  <img align='left' class='img-circle' src="{{url('/images/avatars/')}}/{{$curso->autor->avatar}}" alt="Willian Justen">
                  <div class='small course-author-name'>
                    {{$curso->autor->nombre}}
                  </div>
                  
                </div>
                <!-- BARRA PROGRESIVA (VISIBLE PARA USUARIOS LOGEADOS) -->
                <div class='pull-right hidden'>
                  <div class='small course-progress'>
                    <span class='percentage' >
                      %
                    </span>
                    <br>
                    COMPLETO
                  </div>
                </div>
                <!-- PRECIO (VISIBLE PARA USUARIOS NO LOGEADOS) -->
                <div class='pull-right'>
                  <div class='small course-price'>
                    GRATIS
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
              @endforeach
          </div>



 <!--/LISTA DE CURSOS-->


       <div class="text-center">{!!$cursos->links()!!}</div>




    <br>
  </div>
</div>


@endsection

@section('footer')

@include('partials.footer')

@endsection
