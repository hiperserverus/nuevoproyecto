
  <!-- Navbar -->
  <div class="navbar navbar-fedora navbar-fixed-top is-at-top bs-docs-nav
  is-not-signed-in  full-width half-height " id='navbar' role='navigation'>
  <div class='container'>
    <div class='navbar-header'>
    <button class='navbar-toggle' data-target='.navbar-header-collapse' data-toggle='collapse' type='button'>
      <span class='sr-only'>
      Toggle navigation
      </span>
      <span class='icon-bar'></span>
      <span class='icon-bar'></span>
      <span class='icon-bar'></span>
    </button>
    <!-- Site logo -->
    
      <a class='school-title navbar-brand' href="{{ url('/') }}">
      <img src="{{ url('images/marcador.png') }}" alt="">{{ config('app.name', 'Laravel') }}
      </a>
    

    <!-- Header Menu -->
    <div class='collapse navbar-collapse navbar-header-collapse'>
      <ul class='nav navbar-nav navbar-right'>
  

  

  

  <!-- If more than 5 links, collapse the rest in a dropdown -->
                
                @if (Auth::guest())
                            <li><a class='navbar-link fedora-navbar-link' href="{{ url('/login') }}">Entrar</a></li>
                            <li><a class='btn btn-primary pull-right btn-lg' href="{{ url('/register') }}">Registrarse</a></li>
                          @else


                          

          

            
                
              <li>
                
                <a class='fedora-navbar-link navbar-link' href="{{ url('/cursos') }}" target=''>
                  <i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbspCursos
                </a>
                
              </li>

              <li class="dropdown">
                 

                <a aria-expanded='false' aria-haspopup='true' class='fedora-navbar-link navbar-link dropdown-toggle open-my-profile-dropdown' data-toggle='dropdown'>
                  <i class="fa fa-user" aria-hidden="true"></i>&nbsp{{ Auth::user()->name }}
                  <span class="sr-only">Configuracion de Usuario</span>
                </a>

                <ul class="dropdown-menu" role="menu">

                 <li class='user-profile'>
                      <a href="{{route('usuario.perfil',Auth::user()->name)}}">
                        <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp&nbspEditar Perfil
                      </a>
                </li>
                @if (Auth::user()->tipoUsuario())
                <li>
                      <a href="{{ url('/administrador/usuarios') }}">
                        <i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp&nbspGestionar Usuarios
                      </a>
                </li>
                
                <li>
                      <a href="{{ url('/administrador/categorias') }}">
                       <i class="fa fa-folder-open" aria-hidden="true"></i>&nbsp&nbspGestionar Categorias
                      </a>
                </li>

                <li>
                      <a href="{{ url('/administrador/cursos') }}">
                       <i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp&nbspGestionar Cursos
                      </a>
                </li>

                <li>
                      <a href="{{ url('/administrador/imagenes') }}">
                       <i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp&nbspGestionar imagenes
                      </a>
                </li>

                <li>
                      <a href="{{ url('/administrador/tags') }}">
                       <i class="fa fa-tags" aria-hidden="true"></i>&nbsp&nbspGestionar Tags
                      </a>
                </li>
                @endif

                  <li>
                    <a href="{{ url('/logout') }}"
                      onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                      <i class="fa fa-power-off" aria-hidden="true"></i>&nbsp&nbspSalir
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  </li>
                </ul>
              </li>                        

          @endif
  
  
</ul>

    </div>
    </div>
  </div>
  </div>
