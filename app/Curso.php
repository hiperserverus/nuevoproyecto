<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Categoria;

class Curso extends Model
{

    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'nombre'
            ]
        ];
    }

       public static function findBySlugOrFail($slug, array $columns = ['*']){
        return static::whereSlug($slug)->firstOrFail($columns);
    }


    protected $table = "cursos";
    protected $fillable = ['nombre','descripcion','type','user_id','category_id','autor_id','slug'];

 

    public function categoria() {

    	return $this->belongsTo('App\Categoria','category_id');
    }

    public function user() {

    	return $this->belongsTo('App\User');
    }

        public function autor() {

        return $this->belongsTo('App\Autor');
    }

    public function imagenes() {

    	return $this->hasMany('App\Imagen');
    }

    public function tags() {

    	return $this->belongsToMany('App\Tag');
    }

        public function contenidos () {

        return $this->hasMany('App\Contenido','curso_id');
    }

       public function capitulos (){

        return $this->hasMany('App\Capitulo');
    }


        public function scopeBuscador($query ,$nombre) {

        return $query->where('nombre', 'LIKE', "%$nombre%");

    }

             public function scopeBuscarDestacados($query) {

        return $query->where('type', '=', "destacado");

     }
}
