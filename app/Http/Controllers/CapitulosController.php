<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Capitulo;
use Laracasts\Flash\Flash;
use App\Http\Requests\CapituloRequest;

class CapitulosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$curso_id,$contenido_id)
    {

        $capitulos = Capitulo::BuscarLink($request->link)->BuscarCapitulos($contenido_id)->orderBy('id','ASC')->paginate(10);


        return view('administrador.capitulos.index',['capitulos' => $capitulos,'curso_id' => $curso_id ,'contenido_id' => $contenido_id]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($curso_id,$contenido_id)
    {

        return view('administrador.capitulos.create',['curso_id' => $curso_id, 'contenido_id' => $contenido_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CapituloRequest $request,$curso_id,$contenido_id)
    {
        $capitulo  = new Capitulo($request->all());
        $capitulo->contenido_id = $contenido_id;
        $capitulo->curso_id = $curso_id;

        
        
        
         if($capitulo->save()) {

        Flash::success("Se ha registrado el capitulo: ". $capitulo->nombre . " Exitosamente¡¡");
        return redirect()->route('capitulos.index', [$curso_id,$contenido_id]); 
       
       }else{
        Flash::error("No se pudo crear el capitulo");
         return view('administrador.capitulos.create',['curso_id' => $curso_id, 'contenido_id' => $contenido_id]); 
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($curso_id,$contenido_id,$capitulo_id)
    {
        
         $capitulo = Capitulo::find($capitulo_id);
      

        return view('administrador.capitulos.edit',['curso_id' => $curso_id,'capitulo' => $capitulo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $curso_id,$contenido_id,$capitulo_id)
    {
        $capitulo = Capitulo::find($capitulo_id);
        $capitulo->fill($request->all());



        if($capitulo->save()) {

      
        Flash::success("Se ha actualizado el capitulo : ". $capitulo->nombre . " Exitosamente¡¡");
         return redirect()->route('capitulos.index', [$curso_id,$contenido_id]); 
       
       }else{
        Flash::error("No se pudo actualizar el contenido");
        return redirect()->route('capitulos.edit', [$curso_id,$contenido_id,$capitulo_id]);
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($curso_id,$contenido_id,$capitulo_id)
    {
        $capitulo = Capitulo::find($capitulo_id);
        
        if($capitulo->delete()) {

        Flash::success("Capitulo: " . $capitulo->titulo . " Eliminado Exitosamente¡¡");
            return redirect()->route('capitulos.index', [$curso_id,$contenido_id]);   
       
       }else{
        Flash::error('No se pudo eliminar el capitulo');
            return redirect()->route('capitulos.index', [$curso_id,$contenido_id]); 
       }
    }
}
