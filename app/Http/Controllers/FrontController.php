<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curso;
use App\Categoria;
use App\User;
use App\Autor;
use App\Capitulo;
use App\Http\Requests\PerfilRequest;
use Laracasts\Flash\Flash;

class FrontController extends Controller
{

	public function index(){


		$cursos = Curso::BuscarDestacados()->get();

		   $cursos->each(function($cursos){
    			$cursos->categoria;
    			$cursos->imagenes;
				$cursos->autor;
				$cursos->tags;
    		});

		   return view('home',['cursos'=>$cursos]);

	}

		public function perfil ($name){

		$usuario = User::BuscarUsuario($name)->first();

		return view('usuario',['usuario' => $usuario]);

	}

	public function perfil_update(Request $request, $name) {

		$usuario = User::BuscarUsuario($name)->first();


		if($request->password != null){
			$usuario->fill($request->all());

		}else{

				$usuario->name = $request->name;
				$usuario->email = $request->email;

        		
		}

		if($usuario->save()) {

        Flash::success("Usuario: " . $usuario->name . " Actualizado Exitosamente¡¡");
        return redirect('/perfil/'.$usuario->name);    
       
       }else{
        Flash::error('No se pudo actualizar el usuario');
        return redirect('/perfil/'.$usuario->name);  
       }

		
	

		

	}


    public function cursos(Request $request) {

    			$cursos = Curso::buscador($request->nombre)->orderBy('id','DESC')->simplepaginate(6);
    			$autores = Autor::all();

    			$cursos->each(function($cursos){
    			$cursos->categoria;
    			$cursos->imagenes;
				$cursos->autor;
				$cursos->tags;
    			});

    
				

				//dd($autores);

				return view('cursos.index',['cursos'=>$cursos,'autores'=>$autores]);
		}




	public function buscarCategoria($nombre) {

		$categoria = Categoria::BuscarCategoria($nombre)->first();
		$cursos = $categoria->cursos()->simplepaginate(4);
		

			$cursos->each(function($cursos){
    			$cursos->categoria;
    			$cursos->imagenes;
				$cursos->user;
				$cursos->tags;
    			});

    	return view('cursos.index',['cursos'=>$cursos]);



	}


	public function buscarAutores($name) {

		$autor = Autor::BuscarAutores($name)->first();
		$cursos = $autor->cursos()->simplepaginate(4);
		$autores = Autor::all();

			$cursos->each(function($cursos){
    			$cursos->categoria;
    			$cursos->imagenes;
				$cursos->user;
				$cursos->tags;
    			});

    	return view('cursos.index',['cursos'=>$cursos,'autores'=>$autores]);



	}


		public function contenido($slug){

		$capitulos_disponibles = 0;
		$curso = Curso::findBySlugOrFail($slug);
		$capitulos_disponibles = $curso->capitulos->count();
		$capitulos_activos = Capitulo::BuscarActivos($curso->id)->count();
		$proximo_capitulo = Capitulo::BuscarPendiente($curso->id)->orderBy('id','ASC')->first();
		$imagen = $curso->imagenes->first();

		$curso->contenidos;
		$curso->capitulos;


		
		if($capitulos_disponibles !=0) {

		$porcentaje_curso = floor(($capitulos_activos * 100) / $capitulos_disponibles);
		
		
		}else{
			$porcentaje_curso = 0;
		}

		$curso_name = $curso->nombre;

		return view('cursos.contenido',['curso' => $curso,'curso_name' => $curso_name ,'imagen'=> $imagen , 'porcentaje_curso' => $porcentaje_curso, 'proximo_capitulo' => $proximo_capitulo]);

	}

	public function politicas() {

		return view('politicas');
	}

	public function terminos() {

		return view('terminos');
		
	}


}
