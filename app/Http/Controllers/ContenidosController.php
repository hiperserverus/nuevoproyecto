<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contenido;
use Laracasts\Flash\Flash;
use App\Http\Requests\ContenidoRequest;

class ContenidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($curso_id)
    {
        $contenidos = Contenido::BuscarContenido($curso_id)->orderBy('id','ASC')->paginate(10);



        return view('administrador.contenidos.index',['contenidos' => $contenidos,'curso_id' => $curso_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($curso_id)
    {

        return view('administrador.contenidos.create',['curso_id' => $curso_id]);
           
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContenidoRequest $request,$curso_id)
    {
        $contenido  = new Contenido($request->all());
        $contenido->curso_id = $curso_id;
        
         if($contenido->save()) {

        Flash::success("Se ha registrado el contenido: ". $contenido->titulo . " Exitosamente¡¡");
        return redirect()->route('contenidos.index',$curso_id);
       
       }else{
        Flash::error("No se pudo crear el contenido");
        return redirect()->route('contenidos.create',$curso_id);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_curso,$id_contenido)
    {
        $contenido = Contenido::find($id_contenido);
      

        return view('administrador.contenidos.edit',['contenido' => $contenido]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $curso_id,$contenido_id)
    {
        $contenido = Contenido::find($contenido_id);
        $contenido->fill($request->all());

        if($contenido->save()) {

      
        Flash::success("Se ha actualizado el contenido : ". $contenido->titulo . " Exitosamente¡¡");
         return redirect()->route('contenidos.index',$curso_id);
       
       }else{
        Flash::error("No se pudo actualizar el contenido");
        return redirect()->route('contenidos.edit',[$curso_id,$contenido_id]);
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($curso_id,$contenido_id)
    {
        $contenido = Contenido::find($contenido_id);
        
        if($contenido->delete()) {

        Flash::success("Contenido: " . $contenido->titulo . " Eliminado Exitosamente¡¡");
          return redirect()->route('contenidos.index',$curso_id);   
       
       }else{
        Flash::error('No se pudo eliminar el curso');
           return redirect()->route('contenidos.index',$curso_id); 
       }
    }
}
