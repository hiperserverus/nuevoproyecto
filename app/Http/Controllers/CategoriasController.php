<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Http\Requests\CategoriaRequest;
use App\Categoria;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::orderBy('id','DESC')->simplepaginate(5);

        return view('administrador.categorias.index',['categorias' => $categorias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.categorias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriaRequest $request)
    {
        $categoria = new Categoria($request->all());

        if($categoria->save()) {

        Flash::success("Se ha registrado la categoria: ". $categoria->nombre . " Exitosamente¡¡");
        return redirect()->route('categorias.index');   
       
       }else{
        Flash::error("No se pudo crear el categoria");
        return redirect()->route('categorias.create'); 
       }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoria = Categoria::find($id);

        return view('administrador.categorias.edit',['categoria' => $categoria]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoria = Categoria::find($id);
        $categoria->fill($request->all());

         if($categoria->save()) {

        Flash::success("Categoria: " . $categoria->nombre . " Actualizada Exitosamente¡¡");
        return redirect()->route('categorias.index');
       
       }else{
        Flash::error('No se pudo actualizar la categoria');
        return redirect()->route('categorias.edit',$id); 
       }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoria = Categoria::find($id);

        if($categoria->delete()) {

        Flash::success("Categoria: " . $categoria->nombre . " Eliminado Exitosamente¡¡");
        return redirect()->route('categorias.index');    
       
       }else{
        Flash::error('No se pudo eliminar la categoria');
        return redirect()->route('categorias.index');
       }


    }
}
