<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Autor;
use App\Tag;
use App\Curso;
use App\Imagen;
use Laracasts\Flash\Flash;
use App\User;
use App\Http\Requests\CursoRequest;

class CursosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cursos = Curso::buscador($request->nombre)->orderBy('id','DESC')->paginate(10);

        $cursos->each(function($cursos){

            $cursos->categoria;
            $cursos->user;

        });

        

        return view('administrador.cursos.index',['cursos' => $cursos]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::orderBy('nombre','ASC')->pluck('nombre','id');
         $autores = Autor::all()->pluck('nombre','id');
        $tags = Tag::orderBy('nombre','ASC')->pluck('nombre','id');
        //dd($categorias);
        return view('administrador.cursos.create',['categorias' => $categorias,  'autores' => $autores ,'tags' => $tags]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CursoRequest $request)
    {
        $hasfile = $request->hasFile('imagen') && $request->imagen->isValid();
        
        if( $hasfile){

            $curso  = new Curso($request->all());
            $curso->user_id = \Auth::user()->id;
            
               if($curso->save()) {
                
                $curso->tags()->sync($request->tags);

                $file = $request->file('imagen');
                $extension = $request->imagen->extension();
                $nombre_imagen = str_slug($curso->nombre) . "-" . time() . "." .$extension;

                $imagen = new Imagen();
                $imagen->nombre = $nombre_imagen;
                $imagen->curso()->associate($curso);

                       if($imagen->save()) {

                            $ruta = public_path() . '/images/cursos';
                            $file->move($ruta,$nombre_imagen);   
                       
                       }else{
                        Flash::error("No se pudo guardar la Imagen , Intentelo de nuevo");
                        return redirect()->route('cursos.create');
                       }

                Flash::success("Se ha registrado el curso: ". $curso->nombre . " Exitosamente¡¡");
                return redirect()->route('cursos.index');    
               
               }else{
                Flash::error("No se pudo crear el curso");
                return redirect()->route('cursos.create');
               }

    
        } else {

                Flash::error("Error al cargar la Imagen, Intentelo nuevamente");
                return redirect()->route('cursos.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $curso = Curso::find($id);
        $curso->categoria;

       $categorias = Categoria::orderBy('nombre','DESC')->pluck('nombre','id');
       $autores = Autor::all()->pluck('nombre','id');
       $tags = Tag::orderBy('nombre','DESC')->pluck('nombre','id');
       $my_tags = $curso->tags->pluck('id')->ToArray();



       return view('administrador.cursos.edit',['curso' => $curso, 'categorias' => $categorias, 'autores' => $autores ,'tags' => $tags, 'my_tags' => $my_tags]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $curso = Curso::find($id);
        $curso->fill($request->all());

         if($curso->save()) {

        $curso->tags()->sync($request->tags);

        Flash::success("Se ha actualizado el curso : ". $curso->nombre . " Exitosamente¡¡");
        return redirect()->route('cursos.index');   
       
       }else{
        Flash::error("No se pudo actualizar el curso");
        return redirect()->route('cursos.edit',$id);
       }

        


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $curso = Curso::find($id);
        
        if($curso->delete()) {

        Flash::success("Usuario: " . $curso->nombre . " Eliminado Exitosamente¡¡");
        return redirect()->route('cursos.index');   
       
       }else{
        Flash::error('No se pudo eliminar el curso');
        return redirect()->route('cursos.index'); 
       }
    }
}
