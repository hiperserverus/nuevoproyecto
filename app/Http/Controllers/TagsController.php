<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TagRequest;
use App\Tag;
use Laracasts\Flash\Flash;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        $tags = Tag::buscador($request->nombre)->orderBy('id','DESC')->paginate(10);

        return view('administrador.tags.index',['tags' => $tags]);

          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request)
    {
        $tag = new Tag($request->all());

        if($tag->save()) {

        Flash::success("Se ha registrado el tag: ". $tag->nombre . " Exitosamente¡¡");
        return redirect()->route('tags.index');    
       
       }else{
        Flash::error("No se pudo crear el tag");
        return redirect()->route('tags.create');  
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::find($id);

        return view('administrador.tags.edit',['tag' => $tag]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tag = Tag::find($id);
        $tag->fill($request->all());

        if($tag->save()) {

        Flash::success("tag: " . $tag->nombre . " Actualizado Exitosamente¡¡");
        return redirect()->route('tags.index');      
       
       }else{
        Flash::error('No se pudo actualizar el tag');
        return redirect()->route('tags.edit',$id); 
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag::find($id);

        if($tag->delete()) {

        Flash::success("tag: " . $tag->nombre . " Eliminado Exitosamente¡¡");
        return redirect()->route('tags.index');     
       
       }else{
        Flash::error('No se pudo eliminar el tag');
        return redirect()->route('tags.index');  
       }
    }
}
