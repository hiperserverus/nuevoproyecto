<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = "categorias";
    protected $fillable = ['nombre'];

    public function cursos () {

        return $this->hasMany('App\Curso','category_id');
    }

    public function scopeBuscarCategoria ($query, $nombre) {

    	return $query->where('nombre','=',$nombre);
    }


}
