<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    protected $table = "autores";
    protected $fillable = ['nombre','avatar'];

  public function cursos (){

        return $this->hasMany('App\Curso');
    }

        public function scopeBuscarAutores ($query, $nombre) {

        return $query->where('nombre','=',$nombre);
    }

}
