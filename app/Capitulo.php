<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Capitulo extends Model
{
    protected $table = "capitulos";
    protected $fillable = ['nombre','link','status','curso_id','contenido_id'];

        public function contenido() {

    	return $this->belongsTo('App\Contenido','contenido_id');
    }

    	    public function curso() {

    	return $this->belongsTo('App\Curso');
    }

           public function scopeBuscarCapitulos ($query, $contenido_id) {

    	return $query->where('contenido_id','=',$contenido_id);
    }

            public function scopeBuscarActivos ($query,$curso_id) {

        return $query->where([['status','=',"activo"],['curso_id','=',$curso_id]]);
    }

            public function scopeBuscarPendiente ($query,$curso_id) {

        return $query->where([['status','=',"pendiente"],['curso_id','=',$curso_id]]);
    }

                public function scopeBuscarLink($query,$link) {

        return $query->where('link', 'LIKE', "%$link%");

    }

}