<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function cursos (){

        return $this->hasMany('App\Curso');
    }

     public function scopeBuscarUsuario ($query, $name) {

        return $query->where('name','=',$name);
    }

        public function  tipoUsuario() {

            return $this->type === 'administrador';
        }
}
