<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = "tags";
    protected $fillable = ['nombre'];


    public function cursos() {

    	return $this->belongsToMany('App\Curso')->withTimestamps();
    }

    public function scopeBuscador($query ,$nombre) {

    	return $query->where('nombre', 'LIKE', "%$nombre%");

    }

}
